package com.energizeglobal.internship.xml.model;

import java.util.*;

public class ElementCache {
    private  final Map<String,Element> elementMap = new HashMap<>();

    public  Element getFromCache(String elementName) {
        elementMap.computeIfAbsent(elementName, Element::new);
        return elementMap.get(elementName);
    }

}
