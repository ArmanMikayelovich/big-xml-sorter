package com.energizeglobal.internship.xml.model;


public class ElementLocation {
    private long startLine;
    private long startColumn;

    private long endLine;
    private long endColumn;

    public void setStartLocation(long  startLine, long startColumn) {
        this.startLine = startLine;
        this.startColumn = startColumn;
    }

    public void setEndLocation(long endLine, long endColumn) {
        this.endLine = endLine;
        this.endColumn = endColumn;
    }

    public long getStartLine() {
        return startLine;
    }

    public long getStartColumn() {
        return startColumn;
    }

    public long getEndLine() {
        return endLine;
    }

    public long getEndColumn() {
        return endColumn;
    }

    @Override
    public String toString() {
        return "ElementLocation{" +
                "startLine=" + startLine +
                ", startColumn=" + startColumn +
                ", endLine=" + endLine +
                ", endColumn=" + endColumn +
                '}';
    }
}
