package com.energizeglobal.internship.xml.model;



import java.util.*;

/**
 * Element can be inner element of xml. Example
 *      //book is name of element
 *      //id is an argument of the book
 *      //author and price are child elements.
 *      <book "id"=5 >
 *          <author>Joshua Bloch</author>
 *          <price>-29.99$$</price>
 *          </book>
 *
 * Or Element can contain only some value Example ->  <price>29.99$$</price>
 */
public class Element {
    private final String name;
    private String value;
    private final ElementLocation elementLocation = new ElementLocation();

    private Map<String,String> attributes;

    private Set<Element> children;

    public Element(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public ElementLocation getElementLocation() {
        return elementLocation;
    }

    public Set<Element> getChildren() {
        return children;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void addAttribute(String name, String value) {
        if (attributes == null) {
            this.attributes = new HashMap<>();
        }
        attributes.put(name, value);
    }



    public Map<String, String> getAttributes() {
        return attributes;
    }


    public void addChild(Element child) {
        if (this.children == null) {
            this.children = new LinkedHashSet<>();
        }
        children.add(child);
    }

    @Override
    public boolean equals(Object other) {
        if (this == other) return true;
        if (other == null || getClass() != other.getClass()) return false;
        Element element = (Element) other;
        return name.equals(element.name) && this.attributes.equals(((Element) other).attributes);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return "Element{" +
                "name='" + name + '\'' +
                ", value='" + value + '\'' +
                ", elementLocation=" + elementLocation +
                ", attributes=" + attributes +
                ", children=" + children +
                '}';
    }
}
