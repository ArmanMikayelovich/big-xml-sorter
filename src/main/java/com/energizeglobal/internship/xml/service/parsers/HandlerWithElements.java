package com.energizeglobal.internship.xml.service.parsers;

import com.energizeglobal.internship.xml.model.Element;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public abstract class HandlerWithElements  extends DefaultHandler {
    protected final List<Element> elements = new ArrayList<>();

    public List<Element> getElements() {
        return elements;
    }
}
