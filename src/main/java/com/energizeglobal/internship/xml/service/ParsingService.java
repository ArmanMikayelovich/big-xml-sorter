package com.energizeglobal.internship.xml.service;

import com.energizeglobal.internship.xml.model.Element;
import com.energizeglobal.internship.xml.service.file.ReadService;
import com.energizeglobal.internship.xml.service.parsers.SaxParsingHandler;
import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.Map;

public class ParsingService {
    private static final Logger log = Logger.getLogger(ParsingService.class);
    private ParsingService() {
    }

    public static Element parseFile(String fileName, ReadService readService) throws IOException, SAXException, ParserConfigurationException {

        SAXParserFactory factory = SAXParserFactory.newInstance();

        InputStream xmlInput = readService.getInputStream(fileName);

        SAXParser saxParser = factory.newSAXParser();
        SaxParsingHandler saxParsingHandler = new SaxParsingHandler();
        log.info("Start parsing: " + LocalDateTime.now().toString());
        saxParser.parse(xmlInput, saxParsingHandler);
        log.info("Finish parsing: " + LocalDateTime.now().toString());
        return saxParsingHandler.getElementMap().get(0).get(0);
    }


    public static void printInformation(Element rootElement) {
        String rootName = rootElement.getName();
       log.info("Root element: " + rootName);
        log.info(rootName + " have " + rootElement.getChildren().size() + " type of elements");
        log.info("===================     Showing element types    =====================");
        for (Element element : rootElement.getChildren()) {
            String elementName = element.getName();
            StringBuilder stringBuilder = new StringBuilder();

            if (element.getAttributes() != null) {
                for (Map.Entry<String, String> attribute : element.getAttributes().entrySet()) {

                    stringBuilder.append(attribute.getKey()).append(": ").append(attribute.getValue()).append(", ");
                }
            }

            log.info("\t Element name: " + elementName + ", Element attributes: " + stringBuilder.toString());
            StringBuilder childElementsStringRepresentation = new StringBuilder();
            childElementsStringRepresentation.append("\n");
            for (Element child : element.getChildren()) {
                StringBuilder attributesStringRepresentation = new StringBuilder();
                if (child.getAttributes() != null) {
                    for (Map.Entry<String, String> attribute : child.getAttributes().entrySet()) {
                        attributesStringRepresentation
                                .append(attribute.getKey()).append(": ").append(attribute.getValue()).append(", ");
                    }
                }
                childElementsStringRepresentation.
                        append("\t")
                        .append("\t Element : ")
                        .append(child.getName())
                        .append(", attributes: ")
                        .append(attributesStringRepresentation.toString()).append("\n");
            }
            log.info("\t child elements: " + childElementsStringRepresentation.toString());
        }
    }
}
