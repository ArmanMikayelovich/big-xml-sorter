package com.energizeglobal.internship.xml.service.parsers;

import com.energizeglobal.internship.xml.model.Element;
import com.energizeglobal.internship.xml.model.ElementCache;
import com.energizeglobal.internship.xml.service.util.HashSetImpl;
import org.xml.sax.Attributes;

import java.util.HashMap;
import java.util.Map;

public class SaxParsingHandler extends HandlerWithElements {
    private Element currentElement;
    private final Map<Integer, HashSetImpl<Element>> elementMap = new HashMap<>();
    private int deepCounter = 0;
    private final ElementCache elementCache = new ElementCache();


    public Map<Integer, HashSetImpl<Element>> getElementMap() {
        return elementMap;
    }


    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        Element element = elementCache.getFromCache(qName);
        currentElement = element;

        elements.add(currentElement);
        elementMap.computeIfAbsent(deepCounter, k -> new HashSetImpl<>());
        elementMap.get(deepCounter++).add(element);

        int attributesLength = attributes.getLength();
        if (attributesLength > 0) {
            for (int x = 0; x < attributesLength; x++) {
                currentElement.addAttribute(attributes.getQName(x), attributes.getValue(x));
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        currentElement = elementCache.getFromCache(qName);
        int indexOfLastElement = elements.size() - 1;
        boolean isLastElement = elements.indexOf(currentElement) == indexOfLastElement;
        if (isLastElement) {
            int offset = --deepCounter - 1;
            if (offset < 0) {
                return;
            }
            HashSetImpl<Element> elementSetInOneLevel = elementMap.get(offset);
            int index = elementSetInOneLevel.size() - 1;
            elementSetInOneLevel.get(index).addChild(currentElement);
            elements.remove(currentElement);
        } else {
            elements.remove(currentElement);
            --deepCounter;
        }
    }
}
