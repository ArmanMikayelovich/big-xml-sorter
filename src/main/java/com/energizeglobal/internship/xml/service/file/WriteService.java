package com.energizeglobal.internship.xml.service.file;

import com.energizeglobal.internship.xml.model.Element;
import org.apache.log4j.Logger;

import java.io.*;
import java.time.LocalDateTime;
import java.util.List;

public class WriteService {
    private static final Logger log = Logger.getLogger(WriteService.class);

    private WriteService() {
    }

    public static FileWriter createFileAndGetFileWriter(String fileName) throws IOException {
        log.debug("creating new file with name" + fileName);
        File file = new File(fileName);
        if (!file.createNewFile()) {
            log.error("Can't create file with name:" + fileName);
            throw new IOException("Can't create file with name:" + fileName);
        }
        return new FileWriter(file);
    }

    /**
     * writes element name with all attributes without inner elements or element's end column
     * Example: <text rank="5" author="Bloch" >
     *
     * @param element Element to write header
     * @param writer  where write information
     * @throws IOException when I/O error occurred
     */
    private static void writeElementHeader(Element element, Writer writer) throws IOException {
        StringBuilder attributesBuilder = new StringBuilder();
        if (element.getAttributes() != null) {
            element.getAttributes()
                    .forEach((key, value)
                            -> attributesBuilder.append(" ").append(key).append("=\"").append(value).append("\" "));
        }

        writer.write("<" + element.getName() + " " + attributesBuilder.toString() + ">");
        writer.write(System.lineSeparator());
    }

    private static void writeInnerElementsWithEndOfElement(FileWriter fileWriter, String readingFileName, Element element, ReadService readService) throws IOException {
        String innerElements = readService.readFilePartsToString(readingFileName, element.getElementLocation());
        fileWriter.write(innerElements);
        fileWriter.write(System.lineSeparator());
    }

    private static void writeCloseElementTag(Element element, Writer writer) throws IOException {
        String closeTag = "</" + element.getName() + ">";
        writer.write(System.lineSeparator());
        writer.write(closeTag);
    }

    public static void writeToFile(String writingFileName, String readingFileName, Element headElement, List<Element> sortedElements, ReadService readService) {

        log.info("Starting to  write in file: " + writingFileName);
        FileWriter xmlFileWriter;
        try {
            xmlFileWriter = createFileAndGetFileWriter(writingFileName);
            writeElementHeader(headElement, xmlFileWriter);
            InputStream inputStream = readService.getInputStream(readingFileName);
            inputStream.mark(Integer.MAX_VALUE);
            readService.readLinePositionsFromFile(readingFileName);
            for (int x = 0; x < sortedElements.size(); x++) {
                if ((x % 1000) == 0) {
                    log.info(x + "Elements wrote");
                }
                writeElementHeader(sortedElements.get(x), xmlFileWriter);
                writeInnerElementsWithEndOfElement(xmlFileWriter, readingFileName, sortedElements.get(x), readService);
            }
            writeCloseElementTag(headElement, xmlFileWriter);
            xmlFileWriter.flush();
            log.info("Writing has bees successfully finished: " + writingFileName);
        } catch (FileNotFoundException e) {
            log.error("File with name: " + readingFileName + " not found.");
        } catch (IOException exception) {
            log.error("An I/O error occurred in method writeToFile(String: " + writingFileName + ", " +
                            "String: " + readingFileName + ", Element" + headElement +
                            ", List<Element: " + sortedElements + ", ReadService: readService)"
                    , exception);
        }


    }

}
