package com.energizeglobal.internship.xml.service.util;

import java.util.LinkedHashSet;

public class HashSetImpl<E> extends LinkedHashSet<E> {
    public E get(int index) {
        return (E) this.toArray()[index];
    }
}
