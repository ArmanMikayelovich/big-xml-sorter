package com.energizeglobal.internship.xml.service.util;

public enum SortDirection {
    ASC, DESC
}
