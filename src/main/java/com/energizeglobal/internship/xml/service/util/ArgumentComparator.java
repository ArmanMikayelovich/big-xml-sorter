package com.energizeglobal.internship.xml.service.util;

import com.energizeglobal.internship.xml.model.Element;

import java.util.Comparator;

public class ArgumentComparator implements Comparator<Element> {
    private final String argumentName;

    public ArgumentComparator(String argumentName) {
        this.argumentName = argumentName;
    }

    @Override
    public int compare(Element element1, Element element2) {
        boolean isNumber = true;

        String argument1 = element1.getAttributes().get(argumentName);
        String argument2 = element2.getAttributes().get(argumentName);

        if (argument1 == null && argument2 == null) {
            return 0;
        } else if (argument1 == null) {
            return -1;
        } else if (argument2 == null) {
            return 1;
        }


        Double numberArg1 = null;
        Double numberArg2 = null;
        try {
            numberArg1 = Double.parseDouble(argument1);
            numberArg2 = Double.parseDouble(argument2);
        } catch (NumberFormatException exception) {
            isNumber = false;
        }

        if (isNumber) {
            return numberArg1.compareTo(numberArg2);
        } else {
            return argument1.compareTo(argument2);
        }
    }

    @Override
    public Comparator<Element> reversed() {
        return new ReserveComparator(this);
    }

    private static class ReserveComparator implements Comparator<Element> {
        private final Comparator<Element> comparator;

        private ReserveComparator(Comparator<Element> comparator) {
            this.comparator = comparator;
        }

        @Override
        public int compare(Element o1, Element o2) {
            return comparator.compare(o2, o1);
        }
    }
}
