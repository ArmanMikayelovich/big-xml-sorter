package com.energizeglobal.internship.xml.service.parsers;

import com.energizeglobal.internship.xml.model.Element;
import com.energizeglobal.internship.xml.service.SortingService;
import org.xml.sax.Attributes;
import org.xml.sax.Locator;

public class AttributeSortingHandler extends HandlerWithElements {
    private Locator locator;
    private final Element sortingElement;

    private Element currentElement;

    private final Element rootElement;

    @Override
    public void setDocumentLocator(Locator locator) {
        this.locator = locator;
    }

    public AttributeSortingHandler(Element sortingElement, Element rootElement) {
        this.sortingElement = sortingElement;
        this.rootElement = rootElement;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {

        if (sortingElement.getName().equals(qName) || containsElement(qName)) {
            currentElement = new Element(qName);

            addAllAttributes(attributes);
            SortingService.addElementStartLocation(currentElement, locator.getLineNumber(), locator.getColumnNumber());
        }
    }

    private boolean containsElement(String qName) {
        for (Element element : rootElement.getChildren()) {
            if (element.getName().equals(qName)) {
                return true;
            }
        }
        return false;
    }

    private void addAllAttributes(Attributes attributes) {
        int attributesLength = attributes.getLength();
        if (attributesLength > 0) {
            for (int x = 0; x < attributesLength; x++) {
                currentElement.addAttribute(attributes.getQName(x), attributes.getValue(x));
            }
        }
    }


    @Override
    public void endElement(String uri, String localName, String qName) {
        if (sortingElement.getName().equals(qName) || containsElement(qName)) {
            this.elements.add(currentElement);
            SortingService.addElementEndLocation(currentElement, locator.getLineNumber(), locator.getColumnNumber());
        }

    }

}
