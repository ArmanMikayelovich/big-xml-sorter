package com.energizeglobal.internship.xml.service.util;

import com.energizeglobal.internship.xml.model.Element;

import java.util.Comparator;

public class ElementComparator implements Comparator<Element> {
    private final String elementName;

    public ElementComparator(String elementName) {
        this.elementName = elementName;
    }

    @Override
    public int compare(Element element1, Element element2) {
        boolean isNumber = true;

        String value1 = null;
        String value2 = null;


        for (Element element : element1.getChildren()) {
            if (elementName.equals(element.getName())) {
                value1 = element.getValue();
                break;
            }
        }

        for (Element element : element2.getChildren()) {
            if (elementName.equals(element.getName())) {
                value2 = element.getValue();
                break;
            }
        }

        if (value1 == null && value2 == null) {
            return 0;
        } else if (value1 == null) {
            return -1;
        } else if (value2 == null) {
            return 1;
        }


        Double numberValue1 = null;
        Double numberValue2 = null;
        try {
            numberValue1 = Double.parseDouble(value1);
            numberValue2 = Double.parseDouble(value2);
        } catch (NumberFormatException exception) {
            isNumber = false;
        }
        if (isNumber) {
            return numberValue1.compareTo(numberValue2);
        } else {
            return value1.compareTo(value2);
        }
    }

    @Override
    public Comparator<Element> reversed() {
        return new ElementComparator.ReserveComparator(this);
    }

    private static class ReserveComparator implements Comparator<Element> {
        private final Comparator<Element> comparator;

        private ReserveComparator(Comparator<Element> comparator) {
            this.comparator = comparator;
        }

        @Override
        public int compare(Element o1, Element o2) {
            return comparator.compare(o2, o1);
        }
    }
}
