package com.energizeglobal.internship.xml.service.parsers;

import com.energizeglobal.internship.xml.model.Element;
import com.energizeglobal.internship.xml.service.SortingService;
import org.xml.sax.Attributes;
import org.xml.sax.Locator;

public class ElementSortingHandler extends HandlerWithElements {

    private final Element headElement;
    private final String elementName;
    private final Element rootElement;

    private Locator locator;
    private Element currentElement;
    private Boolean inRootElement;
    private int deep = 0;
    private boolean isTruePosition;

    @Override
    public void setDocumentLocator(Locator locator) {
        this.locator = locator;
    }

    public ElementSortingHandler(Element headElement, String elementName, Element rootElement) {
        this.rootElement = rootElement;
        this.elementName = elementName;
        this.headElement = headElement;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        if (inRootElement == null) {
            inRootElement = rootElement.getName().equals(qName);
        }

        isTruePosition = elementName.equals(qName);

        if (headElement.getName().equals(qName)) {
            currentElement = new Element(qName);
            addAllAttributes(attributes);
            currentElement.addChild(new Element(elementName));
            SortingService.addElementStartLocation(currentElement, locator.getLineNumber(), locator.getColumnNumber());
        }
        if (inRootElement) {
            deep++;
        }
    }

    private void addAllAttributes(Attributes attributes) {
        int attributesLength = attributes.getLength();
        if (attributesLength > 0) {
            for (int x = 0; x < attributesLength; x++) {
                currentElement.addAttribute(attributes.getQName(x), attributes.getValue(x));
            }
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        if (deep == 3 && isTruePosition) {
            currentElement.getChildren().stream()
                    .findFirst().ifPresent(child -> child.setValue(new String(ch, start, length)));
        }

    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if (headElement.getName().equals(qName)) {
            this.elements.add(currentElement);

            SortingService.addElementEndLocation(currentElement, locator.getLineNumber(), locator.getColumnNumber());
        }
        if (inRootElement!= null && inRootElement) {
            deep--;
        }
    }

}
