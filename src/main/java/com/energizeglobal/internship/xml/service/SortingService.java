package com.energizeglobal.internship.xml.service;

import com.energizeglobal.internship.xml.model.Element;
import com.energizeglobal.internship.xml.service.file.ReadService;
import com.energizeglobal.internship.xml.service.parsers.AttributeSortingHandler;
import com.energizeglobal.internship.xml.service.parsers.ElementSortingHandler;
import com.energizeglobal.internship.xml.service.parsers.HandlerWithElements;
import com.energizeglobal.internship.xml.service.util.ArgumentComparator;
import com.energizeglobal.internship.xml.service.util.ElementComparator;
import com.energizeglobal.internship.xml.service.util.SortDirection;
import com.energizeglobal.internship.xml.service.util.SortType;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class SortingService {

    private SortingService() {

    }
    private static List<Element> sort(List<Element> elements, String sortingObjectName,
                                      SortType sortType, SortDirection sortDirection) {

        Comparator<Element> comparator;
        if (sortType.equals(SortType.ARG)) {
            comparator = new ArgumentComparator(sortingObjectName);
        } else {
            comparator = new ElementComparator(sortingObjectName);
        }

        if (sortDirection.equals(SortDirection.ASC)) {
            elements.sort(comparator);
        } else {
            elements.sort(Collections.reverseOrder(comparator));
        }
        return elements;
    }

    public static void addElementStartLocation(Element element, long lineNumber, long columnNumber) {
        element.getElementLocation().setStartLocation(lineNumber, columnNumber);
    }

    public static void addElementEndLocation(Element element, long lineNumber, long columnNumber) {
        element.getElementLocation().setEndLocation(lineNumber, columnNumber);
    }

    public static List<Element> sortXmlElements(String fileName, Element element, String sortingObjectName,
                                         SortType sortType, SortDirection sortDirection,
                                                Element rootElement, ReadService readService)
            throws ParserConfigurationException, SAXException, IOException {

        SAXParserFactory factory = SAXParserFactory.newInstance();

        InputStream xmlInput = readService.getInputStream(fileName);

        SAXParser saxParser = factory.newSAXParser();

        HandlerWithElements sortingHandler;

        if (sortType.equals(SortType.ARG)) {
            sortingHandler = new AttributeSortingHandler(element, rootElement);
        } else {
            sortingHandler = new ElementSortingHandler(element, sortingObjectName,rootElement);
        }

        saxParser.parse(xmlInput, sortingHandler);

        return sort(sortingHandler.getElements(), sortingObjectName, sortType, sortDirection);
    }
}
