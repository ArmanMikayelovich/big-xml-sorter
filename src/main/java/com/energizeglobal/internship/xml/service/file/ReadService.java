package com.energizeglobal.internship.xml.service.file;

import com.energizeglobal.internship.xml.model.Element;
import com.energizeglobal.internship.xml.model.ElementLocation;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;


public class ReadService {
    private static final Logger log = Logger.getLogger(ReadService.class);
    private static final String READ_MODE = "r";

    final Map<Integer, Long> linesPositionsMap = new HashMap<>();

    private Optional<String> retrieveLineSeparator(File file){
        char current;
        String lineSeparator = "";
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            while (fis.available() > 0) {
                current = (char) fis.read();
                if ((current == '\n') || (current == '\r')) {
                    lineSeparator += current;
                    if (fis.available() > 0) {
                        char next = (char) fis.read();
                        if ((next != current)
                                && ((next == '\r') || (next == '\n'))) {
                            lineSeparator += next;
                        }
                    }
                    log.debug("The line separator if file: " + file.getName() + " is " + lineSeparator);
                    return Optional.of(lineSeparator);
                }
            }
        } catch (FileNotFoundException e) {
            log.error("File not found: " + file.getAbsolutePath());
        } catch (IOException exception) {
            log.error("An I/O error occurred: " + exception.getMessage());
        }finally {
            if (fis != null) {
                try{
                    fis.close();
                    log.debug("fileInputStream for file:" + file.getAbsolutePath() +" closed.");
                } catch (IOException exception) {
                    log.error("An I/O error occurred when trying to close FileInputStream for file: "
                            + file.getAbsolutePath());
                }

            }
        }
        log.debug("Can't found separator if file: " + file.getName());
        return Optional.empty();
    }

   public void readLinePositionsFromFile(String fileName) {
        log.debug("Start reading line positions if file " + fileName);
        final Optional<String> lineSeparatorOptional = retrieveLineSeparator(new File(fileName));

        AtomicInteger lineSeparatorLength = new AtomicInteger(0);

        lineSeparatorOptional.ifPresent(str -> lineSeparatorLength.set(str.getBytes().length));

        try (RandomAccessFile randomAccessFile = new RandomAccessFile(fileName, READ_MODE)) {
            int lineNumber = 1;
            while (randomAccessFile.readLine() != null) {
                lineNumber++;
                linesPositionsMap.put(lineNumber, randomAccessFile.getFilePointer() - lineSeparatorLength.get());
            }
            log.debug("Reading line positions if file " + fileName + " successfully finished");

        } catch (IOException exception) {
            log.error("An I/O error occurred in method readLinePositionsFromFile(String: " + fileName + ")"
                    + "\nError message: " + exception.getMessage());
        }

    }


   public String readFilePartsToString(String fileName, ElementLocation location) throws IOException {
        int startLine = (int) location.getStartLine();
        long startColumn = location.getStartColumn() - 1;
        int endLine = (int) location.getEndLine();
        long endColumn = location.getEndColumn() - 1;

        try (RandomAccessFile randomAccessFile = new RandomAccessFile(fileName, READ_MODE)) {
            Long positionOfLine = linesPositionsMap.get(startLine);
            randomAccessFile.seek(positionOfLine + 1);
            StringBuilder stringBuilder = new StringBuilder();
            randomAccessFile.readLine();//skipping empty line
            if (startLine == endLine) {
                final String s = randomAccessFile.readLine();
                stringBuilder.append(s, (int) startColumn , (int) endColumn );
            } else {
                randomAccessFile.readLine();
                for (int x = startLine; x <= endLine; x++) {
                    String s = randomAccessFile.readLine();
                    if (x == startLine && s.length() >= startColumn) {
                        s = s.substring((int) startColumn);
                        stringBuilder.append(s);
                    } else if (x == endLine) {
                        stringBuilder.append(s, 0, (int) endColumn);
                    } else {
                        stringBuilder.append(s);
                    }
                    stringBuilder.append("\n");
                }
            }
            return stringBuilder.toString();
        }
    }

    public static void checkElementName(Element element, String innerElementName) {
        for (Element inner : element.getChildren()) {
            if (innerElementName.equals(inner.getName())) {
                return;
            }
        }
        throw new IllegalArgumentException("Element with name " + innerElementName + " not exists.");
    }

    public static Element getElementFromChildren(String elementName, Element headElement) {

        for (Element element : headElement.getChildren()) {
            if (element.getName().equals(elementName)) {
                return element;
            }
        }
        throw new IllegalArgumentException("Invalid Element name for sort");
    }

    public static void checkAttributesName(Element element, String attributeName) {
        if (!element.getAttributes().containsKey(attributeName)) {
            throw new IllegalArgumentException("Argument not exists.");
        }
    }


    public InputStream getInputStream(String filePath) throws FileNotFoundException {
        log.debug("Creating input stream for file: " + filePath);
        File file = new File(filePath);
        return new FileInputStream(file);
    }
}
