package com.energizeglobal.internship.xml;

import com.energizeglobal.internship.xml.model.Element;
import com.energizeglobal.internship.xml.service.ParsingService;
import com.energizeglobal.internship.xml.service.SortingService;
import com.energizeglobal.internship.xml.service.file.ReadService;
import com.energizeglobal.internship.xml.service.file.WriteService;
import com.energizeglobal.internship.xml.service.util.SortDirection;
import com.energizeglobal.internship.xml.service.util.SortType;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.Scanner;


public class App {
    private static final Logger log = Logger.getLogger(App.class);


    public static void main(String[] args) throws Exception {

        final ReadService readService = new ReadService();
        Scanner scanner = new Scanner(System.in);
        log.info("Enter file path to read XML");

        String readingFileName = scanner.nextLine();
        Element rootElement = ParsingService.parseFile(readingFileName, readService);
        ParsingService.printInformation(rootElement);

        log.info("What elements do you want to sort?");
        String elementName = scanner.nextLine();
        Element element = ReadService.getElementFromChildren(elementName, rootElement);
        log.info("How you want to sort file, with attribute( type ATR )  or with inner elements( type ELM ) ?");
        List<Element> sortedXmlElements;
        switch (scanner.nextLine().trim()) {
            case "ATR": {
                log.info("Please write attribute name.");
                String attributeName = scanner.nextLine();
                ReadService.checkAttributesName(element, attributeName);
                log.info("Choose direction of sorting: \"ASC\" or \"DESC\"?");
                String sortDirection = scanner.nextLine();
                sortedXmlElements = SortingService
                        .sortXmlElements(readingFileName, element, attributeName, SortType.ARG,
                                SortDirection.valueOf(sortDirection), rootElement, readService);
                break;
            }
            case "ELM": {
                log.info("Please write element name.");
                String innerElementName = scanner.nextLine();
                ReadService.checkElementName(element, innerElementName);
                log.info("Choose direction of sorting: \"ASC\" or \"DESC\"?");
                String sortDirection = scanner.nextLine();
                sortedXmlElements = SortingService
                        .sortXmlElements(readingFileName, element, innerElementName, SortType.ELM,
                                SortDirection.valueOf(sortDirection), rootElement, readService);
                break;
            }
            default: {
                log.error("Invalid sorting type.");
                throw new IllegalArgumentException("Invalid sorting type.");
            }
        }
        log.info("Elements sorted. Now type file path for write.");
        String writingFilePath = scanner.nextLine();
        WriteService.writeToFile(writingFilePath, readingFileName, rootElement, sortedXmlElements, readService);


    }


}
